﻿using UnityEngine;
using System.Collections;


public class SpawnPointScript : MonoBehaviour {

    public GameObject objectToSpawn;

    private Vector3 spawnPosition;
    private float spawnRadius = 5f;

	void Start () {
        spawnPosition = transform.position + Random.insideUnitSphere * spawnRadius;
	}

	void Update () {

	}

    public void SpawnObjects() {
        Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);
    }
	
	
}


