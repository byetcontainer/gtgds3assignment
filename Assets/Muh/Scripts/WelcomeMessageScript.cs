﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WelcomeMessageScript : MonoBehaviour {

    private Text welcomeText;

    void Start() {
        welcomeText = GetComponent<Text>();
        Invoke("TurnOffWelcomeMessage", 2f);
    }

    void Update() {
    }

    void TurnOffWelcomeMessage() {
        welcomeText.enabled = false;
    }
	
}


