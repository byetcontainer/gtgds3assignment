﻿using UnityEngine;
using System.Collections;


public class EnemyScript : MonoBehaviour {

    private NavMeshAgent navmeshagent;
    private Collider[] hitColliders;
    private float detectionRadius = 50f;
    public LayerMask detectionLayers;

    private float checkRate;
    private float nextCheck;

	void Start () {
        navmeshagent = GetComponent<NavMeshAgent>();
        checkRate = Random.Range(0.8f, 1.2f);
	}

	void Update () {
        CheckIfPlayerInRange();
	}

    void CheckIfPlayerInRange() {
        if (Time.time > nextCheck && navmeshagent.enabled == true) {
            nextCheck = Time.time + checkRate;
        }

        
        hitColliders = Physics.OverlapSphere(transform.position, detectionRadius, detectionLayers);

        if (hitColliders.Length > 0) {
            navmeshagent.SetDestination(hitColliders[0].transform.position);
        }
    }

	
	
}


